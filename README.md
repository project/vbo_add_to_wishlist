# VBO Add To My Wishlist

VBO Add To My Wishlist module provides the feature of add multiple product items
into my wishlist.

This module will work with VBO, Drupal Commerce and Commerce Wishlist.

you need to enable this module and configure steps which are mentioned below.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/vbo_add_to_wishlist)

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/vbo_add_to_wishlist)


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

- Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- enable this module same as other drupal modules (VBO and Drupal Commerce must
  be enabled)
- Edit your product list view
- Add "field: Global: Views bulk operations" field
- Open SELECTED ACTIONS
- Check "VBO add to wishlist action" and give your label
- save field and save view


## Maintainers

- vishal sheladiya - [vishal.sheladiya](https://www.drupal.org/u/vishalsheladiya)
- Pradeep Kumar Singh - [druprad](https://www.drupal.org/u/druprad)
